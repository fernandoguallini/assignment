Feature: Get all Beers

  @smoke
  Scenario: Check Beers service is up and running
    When I send a get request to the service "BEERS"
    Then the response status code is "200"

  @regression
  Scenario: Get All Beers - Response have id, name, description, abv
    When I send a get request to the service "BEERS"
    Then all items have the attribute "id" with no empty value
    And all items have the attribute "name" with no empty value
    And all items have the attribute "description" with no empty value
    And all items have the attribute "abv" with no empty value