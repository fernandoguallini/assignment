Feature: Get Beers with filters

  Scenario Outline: Get Beers brewed before a certain date - returns only beers which are brewed before given date
    Given the query parameter "brewed_before" with value "<date>"
    When I send a get request to the service "BEERS"
    Then only items with attribute "first_brewed" and date value before "<date>" are returned
    Examples:
      |date|
      |10/2011|
      |02/2005|

  @regression
  Scenario: Get Beers brewed before a certain date - Response have id, name, description, abv
    Given the query parameter "brewed_before" with value "10/2011"
    When I send a get request to the service "BEERS"
    Then all items have the attribute "id" with no empty value
    And all items have the attribute "name" with no empty value
    And all items have the attribute "description" with no empty value
    And all items have the attribute "abv" with no empty value


  Scenario: Get Beers with abv > 6 - returns only beers which have abv > 6
    Given the query parameter "abv_gt" with value "6"
    When I send a get request to the service "BEERS"
    Then all items have the attribute "abv" with value greater than "6"

  @regression
  Scenario: Get Beers with abv > 6 - Response have id, name, description, abv
    Given the query parameter "abv_gt" with value "6"
    When I send a get request to the service "BEERS"
    Then all items have the attribute "id" with no empty value
    And all items have the attribute "name" with no empty value
    And all items have the attribute "description" with no empty value
    And all items have the attribute "abv" with no empty value