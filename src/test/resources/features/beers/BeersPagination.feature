Feature: Get Beers with pagination

  Scenario Outline: Get Beers with pagination - Only returning given number of items
    Given the query parameter "page" with value "2"
    And the query parameter "per_page" with value "<itemsPerPage>"
    When I send a get request to the service "BEERS"
    Then there are "<itemsPerPage>" items in the page
    Examples:
      |itemsPerPage|
      |5|
      |20|

  @regression
  Scenario: Get Beers with pagination - Response have id, name, description, abv
    Given the query parameter "page" with value "2"
    And the query parameter "per_page" with value "5"
    When I send a get request to the service "BEERS"
    Then all items have the attribute "id" with no empty value
    And all items have the attribute "name" with no empty value
    And all items have the attribute "description" with no empty value
    And all items have the attribute "abv" with no empty value
