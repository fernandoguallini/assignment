package assignment.stepdefs;

import assignment.testutil.RestUtil;
import assignment.testutil.Service;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static assignment.testutil.ContainsMatcher.containsCaseInsensitive;
import static assignment.testutil.DateUtil.parseStringToYearMonth;
import static org.hamcrest.Matchers.*;

/**
 * Steps definition
 *
 * @author fernando.guallini
 * @since Sep 2019
 */
public class ServiceSteps {

    private Response response;
    private String queryString = "";
    private RestUtil restUtil = RestUtil.getInstance();
    private List<Map<String, Object>> itemsList = new ArrayList<>();

    private static final String QUERY_STRING_DELIMITER = "&";

    @Given("^the query parameter \"([^\"]*)\" with value \"([^\"]*)\"$")
    public void the_query_parameter_with_value(String key, String value) throws Throwable {
        if (!queryString.isEmpty()) {
            queryString += QUERY_STRING_DELIMITER;
        }
        queryString += String.format("%s=%s", key, value);
    }

    @When("^I send a get request to the service \"([^\"]*)\"$")
    public void i_send_a_get_request_to_the_service(Service service) throws Throwable {
        response = restUtil.get(service, queryString);
    }

    @Then("^the response body contains \"([^\"]*)\"$")
    public void the_response_body_contains(String expectedText) throws Throwable {
        Assert.assertThat(response.asString(), containsCaseInsensitive(expectedText));
    }

    @Then("^the response status code is \"([^\"]*)\"$")
    public void the_response_status_code_is(int expectedCode) throws Throwable {
        Assert.assertThat(response.asString(), response.statusCode(), is(expectedCode));
    }

    @Then("^all items have the attribute \"([^\"]*)\" with value greater than \"([^\"]*)\"$")
    public void all_items_have_the_attribute_with_value_greater_than(String key, Double value) throws Throwable {
        mapItemsToList();
        itemsList.forEach(item -> Assert.assertThat(Double.parseDouble(item.get(key).toString()), greaterThan(value)));
    }

    @Then("^all items have the attribute \"([^\"]*)\" with no empty value$")
    public void all_items_have_the_attribute_with_no_empty_value(String key) throws Throwable {
        if (itemsList.isEmpty()) {
            mapItemsToList();
        }
        itemsList.forEach(item -> Assert.assertThat(item.get(key).toString(), not(isEmptyOrNullString())));
    }

    @Then("^only items with attribute \"([^\"]*)\" and date value before \"([^\"]*)\" are returned$")
    public void only_items_with_attribute_and_date_value_before_are_returned(String attr, String value) throws Throwable {
        mapItemsToList();
        YearMonth expectedDate = parseStringToYearMonth(value);

        itemsList.forEach(item -> {
            YearMonth actualDate = parseStringToYearMonth(item.get(attr).toString());
            String errorMessage = String.format("Expecting only dates before: %s Actual date: %s", expectedDate, actualDate);
            Assert.assertThat(errorMessage, actualDate.isBefore(expectedDate), is(true));
        });
    }

    @Then("^there are \"([^\"]*)\" items in the page$")
    public void there_are_items_in_the_page(int expectedItemsCount) throws Throwable {
        mapItemsToList();
        Assert.assertThat(itemsList.size(), is(expectedItemsCount));
    }

    private void mapItemsToList() {
        JsonPath jsonPath = new JsonPath(response.asString());
        itemsList.addAll(jsonPath.getList(""));
    }
}
