package assignment;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import assignment.testutil.ReportGenerator;
import org.junit.AfterClass;
import org.junit.runner.RunWith;

/**
 * Entry point for test execution
 *
 * @author fernando.guallini
 * @since Sep 2019
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:features",
        plugin = {"pretty", "json:target/CucumberResults.json"},
        monochrome = true
)
public class CucumberTest {

    @AfterClass
    public static void after() {
        Runtime runtime = Runtime.getRuntime();
        runtime.addShutdownHook(new ReportGenerator());
    }
}
