package assignment.testutil;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

/**
 * Utility methods for Dates
 *
 * @author fernando.guallini
 * @since Sep 2019
 */
public class DateUtil {

    private static final String DATE_PATTERN = "MM/yyyy";

    public static YearMonth parseStringToYearMonth(String date) {
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
        return YearMonth.parse(date, dtFormatter);
    }
}
