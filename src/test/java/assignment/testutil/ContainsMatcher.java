package assignment.testutil;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

/**
 * Extension of TypeSafeMatcher for case insensitive
 * match of "contains" method
 *
 * @author fernando.guallini
 * @since Sep 2019
 */
public class ContainsMatcher extends TypeSafeMatcher<String> {

    public static Matcher<String> containsCaseInsensitive(final String expected) {
        return new ContainsMatcher(expected);
    }

    private String expected;

    private ContainsMatcher(String expected) {
        this.expected = expected;
    }

    @Override
    protected boolean matchesSafely(String actual) {
        return actual.toLowerCase().contains(expected);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("should match value ").appendText(expected);
    }
}

