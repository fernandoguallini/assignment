package assignment.testutil;

import java.util.ResourceBundle;

/**
 * Handles environmental configuration
 *
 * @author fernando.guallini
 * @since Sep 2019
 */
class PropertiesBundle {
    private static ResourceBundle rb;

    private static final String DEFAULT_CONTEXT = "prod";

    static String get(String key) {
        String context = System.getProperty("context");
        context = context != null ? context : DEFAULT_CONTEXT;
        if (rb == null) {
            rb = ResourceBundle.getBundle(context);
        }

        return rb.containsKey(key) ? rb.getString(key) : null;
    }
}
