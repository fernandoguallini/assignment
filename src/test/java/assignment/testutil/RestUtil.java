package assignment.testutil;

import com.google.common.base.Strings;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;

/**
 * Utility methods for REST API testing
 *
 * @author fernando.guallini
 * @since Sep 2019
 */
public class RestUtil {
    private static final String BASE_URL = PropertiesBundle.get("baseUrl");
    private static final String QUERY_STRING_SEPARATOR = "?";
    private static final RestUtil restUtil = new RestUtil();

    private RestUtil() {
        RestAssured.baseURI = BASE_URL;
    }

    public static RestUtil getInstance() {
        return restUtil;
    }

    public Response get(Service serviceName, String queryString) {
        String query = serviceName.name();
        if(!Strings.isNullOrEmpty(queryString)) {
            query += QUERY_STRING_SEPARATOR + queryString;
        }

        return RestAssured
                .given().urlEncodingEnabled(true)
                .when().get(query)
                .thenReturn();
    }
}
