package assignment.testutil;

/**
 * ENUM Services to be tested
 *
 * @author fernando.guallini
 * @since Sep 2019
 */
public enum Service {
    BEERS
}
