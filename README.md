# API Testing - Glofox Assignment 

## Prerequisites

Make sure you have these ​dependencies ​installed

```
Maven 3.6​ or above
Java 8​ JDK  
```

## Running the tests

Tests can be run in different environments based on an input parameter (context) that loads the env properties file. 
The default environment is ​prod if no context is passed.
 
Example:

```
mvn clean test -Dcontext=prod 
```
#### Scope
 
Different sets of tests can be executed by tagging the scenarios. 

Example to run only smoke tests:

```
mvn clean test -Dcontext=prod -Dcucumber.options="--tags @smoke" 
```

Or regression tests

```
mvn clean test -Dcontext=prod -Dcucumber.options="--tags @regression" 
```

#### Parallel execution
Tests can be run in parallel by activating "parallel" maven profile. Example:

mvn clean test -Pparallel

#### Execution report
After a test execution, an HTML report with results is generated on the following location:
  
```
target/cucumber-html-reports/overview-features.html 
```

### Project coding style
* [Google Style for Intellij](https://github.com/google/styleguide/blob/gh-pages/intellij-java-google-style.xml/)

## This test automation framework is built using the following tools:
* ​Cucumber​: BDD framework for writing scenarios in Gherkin format 
* Java 8​
* ​Maven​: Dependency management
* Other: RestAssured, cucumber-reporting, junit, maven-surefire-plugin

## Author : Fernando Guallini 